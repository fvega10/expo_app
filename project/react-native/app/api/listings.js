import client from "./client";
import useAuth from "../auth/useAuth";

const endpoint = "/listings";

const getListings = () => client.get(endpoint);

export const addListing = (listing, onUploadProgress) => {
  const { user, logOut } = useAuth();
  const data = new FormData();
  data.append("name", listing.name);
  data.append("username", listing.username);
  data.append("password", listing.password);
  data.append("user_id", user.id);
  data.append("category_id", listing.category.value);
  data.append("link", listing.link);

  return client.post(endpoint, data, {
    onUploadProgress: (progress) =>
      onUploadProgress(progress.loaded / progress.total),
  });
};

export default {
  addListing,
  getListings,
};
