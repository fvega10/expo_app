import React from "react";
import { View, StyleSheet, KeyboardAvoidingView, Platform } from "react-native";

import colors from "../config/colors";
import Text from "../components/Text";

function ListingDetailsScreen({ route }) {
  const listing = route.params;

  return (
    <KeyboardAvoidingView
      behavior="height"
      keyboardVerticalOffset={Platform.OS === "ios" ? 0 : 100}
    >
      <View style={styles.detailsContainer}>
        <Text style={styles.title}>{listing.name}</Text>
        <Text style={styles.subtitle}>Usuario: {listing.username}</Text>
        <Text style={styles.subtitle}>Contraseña: {listing.password}</Text>
        <Text style={styles.subtitle}>Link: {listing.link}</Text>
      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  detailsContainer: {
    marginTop: 30,
    padding: 20,
  },
  subtitle: {
    color: colors.secondary,
    fontWeight: "bold",
    fontSize: 20,
    marginVertical: 10,
  },
  title: {
    fontSize: 24,
    fontWeight: "500",
    textAlign: "center",
    fontWeight: "bold",
  },
  userContainer: {
    marginVertical: 40,
  },
});

export default ListingDetailsScreen;
