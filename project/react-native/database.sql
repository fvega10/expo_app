DROP database IF EXISTS passwhat_db; CREATE database passwhat_db;

CREATE user if NOT EXISTS 'root'@'localhost' IDENTIFIED BY '123456'; 
GRANT ALL PRIVILEGES ON passwhat_db.* TO 'root'@'localhost'; 
FLUSH PRIVILEGES;

USE passwhat_db;

-- Create the tables USE passwhat_db;
CREATE TABLE users (  
	id INT NOT NULL AUTO_INCREMENT,  
	name VARCHAR(100) NOT NULL, 
	email VARCHAR(100) NOT NULL,  
	password VARCHAR(255) NOT NULL,  
PRIMARY KEY (id), UNIQUE KEY username_ukey (email));

CREATE TABLE categories (  
	id INT NOT NULL AUTO_INCREMENT,  
	name VARCHAR(100) NOT NULL,
	icon VARCHAR(100) NOT NULL,
	backgroundColor VARCHAR(100) NOT NULL,
	color VARCHAR(100) NOT NULL,
PRIMARY KEY (id));

CREATE TABLE listings(
    id INT NOT NULL AUTO_INCREMENT,  
	name VARCHAR(100) NOT NULL,
    link VARCHAR(100) NULL,
    user_id INT NOT NULL,
    category_id INT NOT NULL,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY(id),
	CONSTRAINT user_id_listings FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT category_id_listings FOREIGN KEY(category_id) REFERENCES categories(id) ON UPDATE NO ACTION ON DELETE NO ACTION
);


INSERT INTO categories (name, icon, backgroundColor, color) VALUES("Furniture", "floor-lamp", "#fc5c65", "white");
INSERT INTO categories (name, icon, backgroundColor, color) VALUES("Cars", "car", "#fd9644", "white");
INSERT INTO categories (name, icon, backgroundColor, color) VALUES("Cameras", "camera", "#fed330", "white");
