const users = [
  {
    id: 1,
    name: "Fabricio",
    email: "fabriciovu@gmail.com",
    phone: "+50687082379",
    password: "12345",
  },
  {
    id: 2,
    name: "John",
    email: "john@domain.com",
    phone: "+50687082379",
    password: "12345",
  },
  {
    id: 3,
    name: "Ariana",
    email: "ari-0207@hotmail.com",
    phone: "+50687082379",
    password: "12345",
  },
];

const getUsers = () => users;

const getUserById = (id) => users.find((user) => user.id === id);

const getUserByEmail = (email) => users.find((user) => user.email === email);

const addUser = (user) => {
  user.id = users.length + 1;
  users.push(user);
};

module.exports = {
  getUsers,
  getUserByEmail,
  getUserById,
  addUser,
};
