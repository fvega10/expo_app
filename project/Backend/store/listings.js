const listings = [
  {
    id: 201,
    title: "Red jacket",
    images: [{ fileName: "jacket1" }],
    price: 100,
    categoryId: 5,

    userId: 1,
    name: "Instagram",
    username: "fvegau",
    password: "Gj!2321!@#$%",
    link: "www.instagram.com",

    user: {
      userId: 1,
      name: "Fabricio Vega",
      phone: "+50687082379",
      quantity: 5,
    },
    location: {
      latitude: 37.78825,
      longitude: -122.4324,
    },
  },
  {
    id: 2,
    title: "Gray couch in a great condition",
    images: [{ fileName: "couch2" }],
    categoryId: 1,
    price: 1200,

    userId: 1,
    name: "Facebook",
    username: "fvegau",
    password: "123#$%%",
    link: "www.facebook.com",

    user: {
      userId: 1,
      name: "Fabricio Vega",
      phone: "+50687082379",
      quantity: 4,
    },
    location: {
      latitude: 37.78825,
      longitude: -122.4324,
    },
  },

  {
    id: 3,
    title: "Description",
    images: [{ fileName: "couch2" }],
    categoryId: 3,
    price: 600,

    userId: 1,
    name: "Google",
    username: "fvegau",
    password: "asd!@#54",
    link: "www.google.com",

    user: {
      userId: 1,
      name: "Fabricio Vega",
      phone: "+50687082379",
      quantity: 4,
    },
    location: {
      latitude: 37.78825,
      longitude: -122.4324,
    },
  },

];

const addListing = (listing) => {
  listing.id = listings.length + 1;
  listings.push(listing);
};

const getListings = () => listings;

const getListing = (id) => listings.find((listing) => listing.id === id);

const filterListings = (predicate) => listings.filter(predicate);

const getListingsForUser = (toUserId) =>
  listings.filter((message) => message.user.userId === toUserId);

const getListingsByCategory = (id) =>
  listings.filter((message) => message.categoryId === id);

module.exports = {
  addListing,
  getListings,
  getListing,
  filterListings,
  getListingsForUser,
  getListingsByCategory,
};
