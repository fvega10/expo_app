# Pasos para compilar la aplicaciòn:

1. Correr el backend:

- Lo primero es abrir el backend ubicado en la carpeta: *Backend* en visual studio code

- Una vez abierto dicho proyecto correr el siguiente comando en una terminal:

```
npm run
``` 

- Con esto se levanta el servidor en el puerto 9000

2. Correr la aplicación:

- Ejecutar el comando en caso de no tener la carpeta *node_modules*:

```
npm install
```

- Correr la aplicación con el comando: 

```
expo start
```

- En caso que no ejecute por la versiòn del expo-cli, se debe correr uno a uno los siguietne comandos:

```
expo update

npm install -g expo-cli
```


Una vez abierta la aplicación este levantará una ventana de algun navegador, mostrando el codigo QR, este se debe de escanear con al aplicacion de Expo.

Cuando se escanee, se levanta la aplicacion en el celular, hay que dar tiempo para que cargue el: Javascript-bundle


# Comandos necesarios para la progrmación y creación de objetos de expo

## All pachages

```
npm i expo-constants formik@2.1.4 yup @react-navigation/native @react-navigation/stack @react-navigation/bottom-tabs apisauce react-native-progress jwt-decode @react-native-community/netinfo dayjs

expo install react-native-gesture-handler expo-permissions react-native-gesture-handler react-native-reanimated react-native-screens react-native-safe-area-context @react-native-community/masked-view lottie-react-native expo-secure-store

```

## Expo

```
npm i -g expo-cli
expo init DoneWithIt
```

## Lists

```
npm i expo-constants
expo install react-native-gesture-handler
```

## Forms

```
npm i formik@2.1.4
npm i yup
```

## ImagePicker

```
npm i react-native-expo-image-cache
npm i expo-blur
expo install expo-image-picker
```

## Permissions

```
expo install expo-permissions
```

## Navigation
 ```
 npm install @react-navigation/native
 expo install react-native-gesture-handler react-native-reanimated react-native-screens react-native-safe-area-context @react-native-community/masked-view
 ```
 
 ```
 npm install @react-navigation/stack
 ```
 
 ## Tabs
 
 ```
 npm install @react-navigation/bottom-tabs
 ```
 
 ## API Sauce
 
 ```
 npm i apisauce
 ```
 ## Loading indicator
 Web Site: *https://lottiefiles.com/*
 
 ```
 expo install lottie-react-native
 ```
 
 ## Progress bar
 
 ```
 npm install react-native-progress --save
 ```
 
 ## Token
 
 ```
 npm i jwt-decode
 expo install expo-secure-store
 ```
 ## Offline
 
 ```
 npm install --save @react-native-community/netinfo
 ```
 ## Cache
 
 ```
 npm i dayjs
 ```
 
 ## Bugs
 
 ```
 npx bugsnag-expo-cli init
 ```